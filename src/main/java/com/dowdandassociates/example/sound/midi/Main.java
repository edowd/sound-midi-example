/*
 *
 * sound-midi-example/src/main/java/com/dowdandassociates/example/sound/midi/Main.java
 *
 *------------------------------------------------------------------------------
 * Copyright 2015-2018 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

package com.dowdandassociates.example.sound.midi;

import java.io.File;
import java.net.URL;
import java.util.List;

import javax.sound.midi.MidiFileFormat;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.Sequence;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.MissingArgumentException;
import org.apache.commons.cli.MissingOptionException;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

public class Main
{
    private static final String HELP_CMD_LINE_SYNTAX = "sound-midi [OPTIONS] [FILE]";

    public static void main(String[] args)
    {
        Option helpOption = Option.builder("?").
                longOpt("help").
                desc("Print this help message").
                build();

        Option urlOption = Option.builder("u").
                longOpt("url").
                desc("File path is a URL").
                build();

        Option playOption = Option.builder("p").
                longOpt("play").
                desc("Play file").
                build();

        Options options = new Options();
        options.addOption(helpOption);
        options.addOption(urlOption);
        options.addOption(playOption);

        HelpFormatter formatter = new HelpFormatter();

        CommandLineParser parser = new DefaultParser();

        try
        {
            CommandLine cmdLine = parser.parse(options, args);

            if (cmdLine.hasOption("?"))
            {
                formatter.printHelp(HELP_CMD_LINE_SYNTAX, options);
                System.exit(0);
            }

            List<String> remainingArgs = cmdLine.getArgList();
            if (remainingArgs.isEmpty())
            {
                throw new MissingArgumentException("FILE not set");
            }

            MidiFileFormat midiFileFormat;
            Sequence sequence;

            String path = remainingArgs.get(0);
            if (cmdLine.hasOption("u"))
            {
                System.out.println("Open URL: " + path);
                URL inputUrl = new URL(path);
                midiFileFormat = MidiSystem.getMidiFileFormat(inputUrl);
                sequence = MidiSystem.getSequence(inputUrl);
            }
            else
            {
                System.out.println("Open File: " + path);
                File inputFile = new File(path);
                midiFileFormat = MidiSystem.getMidiFileFormat(inputFile);
                sequence = MidiSystem.getSequence(inputFile);
            }

            MidiPismo.printMidiFileFormatInfo(midiFileFormat);
            MidiPismo.printSequenceInfo(sequence);
            MidiPismo.printSequence(sequence);

            if (cmdLine.hasOption("p"))
            {
                MidiPismo.playMidi(sequence);
            }
        }
        catch (MissingOptionException | MissingArgumentException mxe)
        {
            System.out.println(mxe.getMessage());
            formatter.printHelp(HELP_CMD_LINE_SYNTAX, options);
            System.exit(1);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.exit(1);
        }
    }
}

