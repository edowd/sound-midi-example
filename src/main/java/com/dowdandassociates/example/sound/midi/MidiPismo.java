/*
 *
 * sound-midi-example/src/main/java/com/dowdandassociates/example/sound/midi/MidiPismo.java
 *
 *------------------------------------------------------------------------------
 * Copyright 2015-2018 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

package com.dowdandassociates.example.sound.midi;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MetaMessage;
import javax.sound.midi.MidiEvent;
import javax.sound.midi.MidiFileFormat;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Sequence;
import javax.sound.midi.Sequencer;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.SysexMessage;
import javax.sound.midi.Track;

public class MidiPismo
{
    private static final String[] NOTE_NAMES = {"C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"};

    public static void playMidi(Sequence sequence) throws InvalidMidiDataException, MidiUnavailableException
    {
        System.out.println();
        System.out.println("Playing MIDI");

        Sequencer sequencer = MidiSystem.getSequencer();
        sequencer.setSequence(sequence);
        sequencer.open();
        sequencer.start();

        try
        {
            while (sequencer.isRunning())
            {
                Thread.sleep(1000);
            }
        }
        catch (InterruptedException ie)
        {
        }

        sequencer.stop();
        sequencer.close();
    }

    public static void printMidiFileFormatInfo(MidiFileFormat midiFileFormat)
    {
        System.out.println();
        System.out.println("Midi File Format Info");
        System.out.println("---------------------");
        System.out.println("Byte Length: " + midiFileFormat.getByteLength());
        System.out.println("Division Type: " + midiFileFormat.getDivisionType());
        System.out.println("Microsecond Length: " + midiFileFormat.getMicrosecondLength());
        System.out.println("Resolution: " + midiFileFormat.getResolution());
        System.out.println("Type: " + midiFileFormat.getType());
        System.out.println("Properties: " + midiFileFormat.properties());
    }

    public static void printSequenceInfo(Sequence sequence)
    {
        System.out.println();
        System.out.println("Sequence Info");
        System.out.println("-------------");
        System.out.println("Division Type: " + sequence.getDivisionType());
        System.out.println("Microsecond Length: " + sequence.getMicrosecondLength());
        System.out.println("Resolution: " + sequence.getResolution());
        System.out.println("Tick Length: " + sequence.getTickLength());
        System.out.println("Tracks: " + sequence.getTracks().length);
        System.out.println("Patches: " + sequence.getPatchList().length);
    }

    public static void printSequence(Sequence sequence)
    {
        SortedMap<Long, List<EventContainer>> processedSequence = processSequence(sequence);
        printProcessedSequence(processedSequence);
    }

    // Reorganize sequence from tracks to ticks
    private static SortedMap<Long, List<EventContainer>> processSequence(Sequence sequence)
    {
        SortedMap<Long, List<EventContainer>> messages = new TreeMap<>();

        int trackNumber = 0;
        for (Track track : sequence.getTracks())
        {
            ++trackNumber;

            for (int trackIndex = 0, len = track.size(); trackIndex < len; ++trackIndex)
            {
                MidiEvent event = track.get(trackIndex);
                Long tick = event.getTick();
                MidiMessage message = event.getMessage();

                EventContainer container = new EventContainer(sequence, track, trackNumber, trackIndex, event, message);

                if (messages.containsKey(tick))
                {
                    List<EventContainer> eventList = messages.get(tick);
                    eventList.add(container);
                }
                else
                {
                    List<EventContainer> eventList = new ArrayList<>();
                    eventList.add(container);
                    messages.put(tick, eventList);
                }
            }
        }

        return messages;
    }

    private static void printProcessedSequence(SortedMap<Long, List<EventContainer>> events)
    {
        System.out.println();
        System.out.println("Processed Sequence");
        System.out.println("------------------");

        for (Map.Entry<Long, List<EventContainer>> entry : events.entrySet())
        {
            Long tick = entry.getKey();
            List<EventContainer> eventList = entry.getValue();

            for (EventContainer container : eventList)
            {
                Sequence sequence = container.getSequence();
                Track track = container.getTrack();
                int trackNumber = container.getTrackNumber();
                int trackIndex = container.getTrackIndex();
                MidiEvent event = container.getEvent();
                MidiMessage message = container.getMessage();

                System.out.print(String.format("%d\tTrack %2d\t", tick, trackNumber));

                if (message instanceof ShortMessage)
                {
                    printShortMessage((ShortMessage)message);
                }
                else if (message instanceof MetaMessage)
                {
                    printMetaMessage((MetaMessage)message);
                }
                else if (message instanceof SysexMessage)
                {
                    printSysexMessage((SysexMessage)message);
                }
                else
                {
                    printMessage(message);
                }

                System.out.print('\t');
                System.out.print('[');
                System.out.print(message.getClass());
                System.out.print("; ");
                System.out.print("Status: " + message.getStatus());
                System.out.print("; ");
                System.out.print("Length: " + message.getLength());
                System.out.print("; ");
                System.out.print("Message:");
                byte[] data = message.getMessage();
                for (int i = 0; i < data.length; ++i)
                {
                    System.out.print(" ");
                    System.out.print(String.format("%02X", data[i]));
                }
                System.out.print(']');

                System.out.println();
            }
        }
    }

    private static void printMessage(MidiMessage message)
    {
        System.out.print("MidiMessage");
    }

    private static String formatCommand(int command)
    {
        return String.format(" (%d)", command);
    }

    private static int getOctave(int key)
    {
        return (key / 12) - 1;
    }

    private static String getNoteName(int key)
    {
        return NOTE_NAMES[key % 12];
    }

    private static String getNoteNumber(int data1)
    {
        StringBuilder strbuf = new StringBuilder();
        String noteName = getNoteName(data1);
        String octave = String.format("%d", getOctave(data1));
        strbuf.append(noteName);
        strbuf.append(octave);
        if (noteName.length() == 1)
        {
            strbuf.append(' ');
        }
        if (octave.length() == 1)
        {
            strbuf.append(' ');
        }
        strbuf.append(" (Note Number: ");
        strbuf.append(String.format("%3d", data1));
        strbuf.append(')');
        return strbuf.toString();
    }

    private static void printShortMessage(ShortMessage sm)
    {
        int command = sm.getCommand();

        System.out.print(String.format("Channel %2d\t", sm.getChannel()));

        // http://www.midi.org/techspecs/midimessages.php
        // http://web.archive.org/web/20160112192415/http://www.midi.org/techspecs/midimessages.php
        // https://www.midi.org/specifications/category/reference-tables
        // https://www.midi.org/specifications/item/table-2-expanded-messages-list-status-bytes
        switch (command)
        {
        case ShortMessage.ACTIVE_SENSING: // 0xFE, 254
            // http://www.recordingblogs.com/sa/tabid/88/Default.aspx?topic=MIDI+Active+Sense+message
            System.out.print("Active Sensing");
            break;
        case ShortMessage.CHANNEL_PRESSURE: // 0xD0, 208
            // www.recordingblogs.com/sa/tabid/88/Default.aspx?topic=MIDI+Channel+Pressure+message
            System.out.print("Channel Aftertouch");
            System.out.print('\t');
            System.out.print("Pressure: " + sm.getData1());
            break;
        case ShortMessage.CONTINUE: // 0xFB, 251
            // http://www.recordingblogs.com/sa/tabid/88/Default.aspx?topic=MIDI+Continue+message
            System.out.print("Continue");
            break;
        case ShortMessage.CONTROL_CHANGE: // 0XB0, 176
            printControlModeChange(sm);
            break;
        case ShortMessage.END_OF_EXCLUSIVE: // 0xF7, 247
            // https://docs.oracle.com/javase/7/docs/api/javax/sound/midi/SysexMessage.html
            System.out.print("End of SysEx (EOX)");
            break;
        case ShortMessage.MIDI_TIME_CODE: // 0xF1, 241
            // http://www.recordingblogs.com/sa/tabid/88/Default.aspx?topic=MIDI+Quarter+Frame+message
            System.out.print("MIDI Time Code Qtr. Frame");
            System.out.print('\t');
            System.out.print("2nd Byte: " + sm.getData1());
            System.out.print('\t');
            System.out.print("3rd Byte: " + sm.getData2());
            break;
        case ShortMessage.NOTE_OFF: // 0x80, 128
            // www.recordingblogs.com/sa/tabid/88/Default.aspx?topic=MIDI+Note+Off+message
            System.out.print("Note off");
            System.out.print('\t');
            System.out.print(getNoteNumber(sm.getData1()));
            System.out.print('\t');
            System.out.print(String.format("Note Velocity: %3d", sm.getData2()));
            break;
        case ShortMessage.NOTE_ON: // 0x90, 144
            // http://www.recordingblogs.com/sa/tabid/88/Default.aspx?topic=MIDI+Note+On+message
            System.out.print("Note on ");
            System.out.print('\t');
            System.out.print(getNoteNumber(sm.getData1()));
            System.out.print('\t');
            System.out.print(String.format("Note Velocity: %3d", sm.getData2()));
            break;
        case ShortMessage.PITCH_BEND: // 0xE0, 224
            // http://www.recordingblogs.com/sa/tabid/88/Default.aspx?topic=MIDI+Pitch+Wheel+message
            System.out.print("Pitch Bend Change");
            System.out.print('\t');
            System.out.print("Pitch Bender LSB: " + sm.getData1());
            System.out.print('\t');
            System.out.print("Pitch Bender MSB: " + sm.getData2());
            break;
        case ShortMessage.POLY_PRESSURE: // 0xA0, 160
            // http://www.recordingblogs.com/sa/tabid/88/Default.aspx?topic=MIDI+Key+Pressure+message
            System.out.print("Polyphonic Aftertouch");
            System.out.print('\t');
            System.out.print(getNoteNumber(sm.getData1()));
            System.out.print('\t');
            System.out.print(String.format("Pressure: %3d", sm.getData2()));
            break;
        case ShortMessage.PROGRAM_CHANGE: // 0xC0, 192
            // http://www.recordingblogs.com/sa/tabid/88/Default.aspx?topic=MIDI+Program+Change+message
            System.out.print("Program Change");
            System.out.print('\t');
            System.out.print("Program #: " + sm.getData1());
            break;
        case ShortMessage.SONG_POSITION_POINTER: // 0xF2, 242
            // http://www.recordingblogs.com/sa/tabid/88/Default.aspx?topic=MIDI+Song+Position+Pointer+message
            System.out.print("Song Position Pointer");
            System.out.print('\t');
            System.out.print("LSB: " + sm.getData1());
            System.out.print('\t');
            System.out.print("MSB: " + sm.getData2());
            break;
        case ShortMessage.SONG_SELECT: // 0xF3, 243
            // http://www.recordingblogs.com/sa/tabid/88/Default.aspx?topic=MIDI+Song+Request+message
            System.out.print("Song Select");
            System.out.print('\t');
            System.out.print("Song #: " + sm.getData1());
            break;
        case ShortMessage.START: // 0xFA, 250
            // http://www.recordingblogs.com/sa/tabid/88/Default.aspx?topic=MIDI+Start+message
            System.out.print("Start");
            break;
        case ShortMessage.STOP: // 0xFC, 252
            // http://www.recordingblogs.com/sa/tabid/88/Default.aspx?topic=MIDI+Stop+message
            System.out.print("Stop");
            break;
        case ShortMessage.SYSTEM_RESET: // 0xFF, 255
            // http://www.recordingblogs.com/sa/tabid/88/Default.aspx?topic=MIDI+Reset+message
            System.out.print("System Reset");
            break;
        case ShortMessage.TIMING_CLOCK: // 0xF8, 248
            // http://www.recordingblogs.com/sa/tabid/88/Default.aspx?topic=MIDI+Clock+message
            System.out.print("Timing clock");
            break;
        case ShortMessage.TUNE_REQUEST: // 0xF6, 246
            // http://www.recordingblogs.com/sa/tabid/88/Default.aspx?topic=MIDI+Tune+Request+message
            System.out.print("Tune Request");
            break;
        default:
            System.out.print("Command: " + sm.getCommand());
            System.out.print('\t');
            System.out.print("2nd Byte: " + sm.getData1());
            System.out.print('\t');
            System.out.print("3rd Byte: " + sm.getData2());
        }
    }

    // https://www.midi.org/specifications/item/table-3-control-change-messages-data-bytes-2
    // http://www.recordingblogs.com/sa/tabid/88/Default.aspx?topic=MIDI+Controller+message
    private static void printControlModeChange(ShortMessage message)
    {
        int controlNumber = message.getData1();
        String thirdByteValue = String.format("%3d", message.getData2());

        String usedAs;
        if ((controlNumber >= 0 && controlNumber <= 31) || controlNumber == 99 || controlNumber == 101)
        {
            usedAs = "MSB";
        }
        else if ((controlNumber >= 32 && controlNumber <= 63) || (controlNumber >= 70 && controlNumber <= 84) || controlNumber == 88 || controlNumber == 98 || controlNumber == 100)
        {
            usedAs = "LSB";
        }
        else
        {
            usedAs = "---";
        }

        if ((controlNumber >= 85 && controlNumber <= 87) ||
                controlNumber == 89 ||
                controlNumber == 90 ||
                (controlNumber >= 102 && controlNumber <= 119))
        {
            thirdByteValue = "---";
        }

        String controlFunction;
        switch (controlNumber)
        {
        case 0:
            controlFunction = "Bank Select";
            break;
        case 1:
            controlFunction = "Modulation Wheel or Lever";
            break;
        case 2:
            controlFunction = "Breath Controller";
            break;
        case 4:
            controlFunction = "Foot Controller";
            break;
        case 5:
            controlFunction = "Portamento Time";
            break;
        case 6:
            controlFunction = "Data Entry MSB";
            break;
        case 7:
            controlFunction = "Channel Volume (formerly Main Volume)";
            break;
        case 8:
            controlFunction = "Balance";
            break;
        case 10:
            controlFunction = "Pan";
            break;
        case 11:
            controlFunction = "Expression Controller";
            break;
        case 12:
            controlFunction = "Effect Control 1";
            break;
        case 13:
            controlFunction = "Effect Control 2";
            break;
        case 16:
            controlFunction = "General Purpose Controller 1";
            break;
        case 17:
            controlFunction = "General Purpose Controller 2";
            break;
        case 18:
            controlFunction = "General Purpose Controller 3";
            break;
        case 19:
            controlFunction = "General Purpose Controller 4";
            break;
        case 32:
            controlFunction = "LSB for Control 0 (Bank Select)";
            break;
        case 33:
            controlFunction = "LSB for Control 1 (Modulation Wheel or Level)";
            break;
        case 34:
            controlFunction = "LSB for Control 2 (Breath Controller)";
            break;
        case 35:
            controlFunction = "LSB for Control 3 (Undefined)";
            break;
        case 36:
            controlFunction = "LSB for Control 4 (Foot Controller)";
            break;
        case 37:
            controlFunction = "LSB for Control 5 (Portamento Time)";
            break;
        case 38:
            controlFunction = "LSB for Control 6 (Data Entry)";
            break;
        case 39:
            controlFunction = "LSB for Control 7 (Channel Volume, formerly Main Volume)";
            break;
        case 40:
            controlFunction = "LSB for Control 8 (Balance)";
            break;
        case 41:
            controlFunction = "LSB for Control 9 (Undefined)";
            break;
        case 42:
            controlFunction = "LSB for Control 10 (Pan)";
            break;
        case 43:
            controlFunction = "LSB for Control 11 (Expression Controller)";
            break;
        case 44:
            controlFunction = "LSB for Control 12 (Effect Control 1)";
            break;
        case 45:
            controlFunction = "LSB for Control 13 (Effect Control 2)";
            break;
        case 46:
            controlFunction = "LSB for Control 14 (Undefined)";
            break;
        case 47:
            controlFunction = "LSB for Control 15 (Undefined)";
            break;
        case 48:
            controlFunction = "LSB for Control 16 (General Purpose Controller 1)";
            break;
        case 49:
            controlFunction = "LSB for Control 17 (General Purpose Controller 2)";
            break;
        case 50:
            controlFunction = "LSB for Control 18 (General Purpose Controller 3)";
            break;
        case 51:
            controlFunction = "LSB for Control 19 (General Purpose Controller 4)";
            break;
        case 52:
            controlFunction = "LSB for Control 20 (Undefined)";
            break;
        case 53:
            controlFunction = "LSB for Control 21 (Undefined)";
            break;
        case 54:
            controlFunction = "LSB for Control 22 (Undefined)";
            break;
        case 55:
            controlFunction = "LSB for Control 23 (Undefined)";
            break;
        case 56:
            controlFunction = "LSB for Control 24 (Undefined)";
            break;
        case 57:
            controlFunction = "LSB for Control 25 (Undefined)";
            break;
        case 58:
            controlFunction = "LSB for Control 26 (Undefined)";
            break;
        case 59:
            controlFunction = "LSB for Control 27 (Undefined)";
            break;
        case 60:
            controlFunction = "LSB for Control 28 (Undefined)";
            break;
        case 61:
            controlFunction = "LSB for Control 29 (Undefined)";
            break;
        case 62:
            controlFunction = "LSB for Control 30 (Undefined)";
            break;
        case 63:
            controlFunction = "LSB for Control 31 (Undefined)";
            break;
        case 64:
            controlFunction = "Damper Pedal on/off (Sustain)";
            if (message.getData2() <= 63)
            {
                thirdByteValue = "off";
            }
            else
            {
                thirdByteValue = "on";
            }
            break;
        case 65:
            controlFunction = "Portamento On/Off";
            if (message.getData2() <= 63)
            {
                thirdByteValue = "off";
            }
            else
            {
                thirdByteValue = "on";
            }
            break;
        case 66:
            controlFunction = "Sostenuto On/Off";
            if (message.getData2() <= 63)
            {
                thirdByteValue = "off";
            }
            else
            {
                thirdByteValue = "on";
            }
            break;
        case 67:
            controlFunction = "Soft Pedal On/Off";
            if (message.getData2() <= 63)
            {
                thirdByteValue = "off";
            }
            else
            {
                thirdByteValue = "on";
            }
            break;
        case 68:
            controlFunction = "Legato Footswitch";
            if (message.getData2() <= 63)
            {
                thirdByteValue = "Normal";
            }
            else
            {
                thirdByteValue = "Legato";
            }
            break;
        case 69:
            controlFunction = "Hold 2";
            if (message.getData2() <= 63)
            {
                thirdByteValue = "off";
            }
            else
            {
                thirdByteValue = "on";
            }
            break;
        case 70:
            controlFunction = "Sound Controller 1 (default: Sound Variation)";
            break;
        case 71:
            controlFunction = "Sound Controller 2 (default: Tibre/Harmonic Intens.)";
            break;
        case 72:
            controlFunction = "Sound Controller 3 (default: Release Time)";
            break;
        case 73:
            controlFunction = "Sound Controller 4 (default: Attack Time)";
            break;
        case 74:
            controlFunction = "Sound Controller 5 (default: Brightness)";
            break;
        case 75:
            controlFunction = "Sound Controller 6 (default: Decay Time - see MMA RP-021)";
            break;
        case 76:
            controlFunction = "Sound Controller 7 (default: Vibrato Rate - see MMA RP-021)";
            break;
        case 77:
            controlFunction = "Sound Controller 8 (default: Vibrato Depth - see MMA RP-021)";
            break;
        case 78:
            controlFunction = "Sound Controller 9 (default: Vibrato Delay - see MMA RP-021)";
            break;
        case 79:
            controlFunction = "Sound Controller 10 (default: undefined - see MMA RP-021)";
            break;
        case 80:
            controlFunction = "General Purpose Controller 5";
            break;
        case 81:
            controlFunction = "General Purpose Controller 6";
            break;
        case 82:
            controlFunction = "General Purpose Controller 7";
            break;
        case 83:
            controlFunction = "General Purpose Controller 8";
            break;
        case 84:
            controlFunction = "Portamento Control";
            break;
        case 88:
            controlFunction = "High Resolution Velocity Prefix";
            break;
        case 91:
            controlFunction = "Effects 1 Depth (default: Reverb Send Level - see MMA RP-023) (formerly External Effects Depth)";
            break;
        case 92:
            controlFunction = "Effects 2 Depth (formerly Tremolo Depth)";
            break;
        case 93:
            controlFunction = "Effects 3 Depth (default: Chorus Send Level - See MMA RP-023) (formerly Chorus Depth)";
            break;
        case 94:
            controlFunction = "Effects 4 Depth (formerly Celeste [Detune] Depth)";
            break;
        case 95:
            controlFunction = "Effects 5 Depth (formerly Phaser Depth)";
            break;
        case 96:
            controlFunction = "Data Increment (Data Entry + 1)(see MMA RP-018)";
            thirdByteValue = "N/A";
            break;
        case 97:
            controlFunction = "Data Increment (Data Entry - 1)(see MMA RP-018)";
            thirdByteValue = "N/A";
            break;
        case 98:
            controlFunction = "Non-Registered Parameter Number (NRPN) - LSB";
            break;
        case 99:
            controlFunction = "Non-Registered Parameter Number (NRPN) - MSB";
            break;
        case 100:
            controlFunction = "Registered Parameter Number (RPN) - LSB";
            break;
        case 101:
            controlFunction = "Registered Parameter Number (RPN) - MSB";
            break;
        case 120:
            controlFunction = "[Channel Mode Message] All Sound Off";
            break;
        case 121:
            controlFunction = "[Channel Mode Message] Reset All Controllers";
            break;
        case 122:
            controlFunction = "[Channel Mode Message] Local Control On/Off";
            if (message.getData2() == 0)
            {
                thirdByteValue = "off";
            }
            else if (message.getData2() == 127)
            {
                thirdByteValue = "on";
            }
            break;
        case 123:
            controlFunction = "[Channel Mode Message] All Notes Off";
            break;
        case 124:
            controlFunction = "[Channel Mode Message] Omni Mode Off (+ all notes off)";
            break;
        case 125:
            controlFunction = "[Channel Mode Message] Omni Mode On (+ all notes off)";
            break;
        case 126:
            controlFunction = "[Channel Mode Message] Mono Mode On (+ poly off, + all notes off)";
            break;
        case 127:
            controlFunction = "[Channel Mode Message] Poly Mode On (+ mono off, + all notes off)";
            break;
        default:
            controlFunction = "Undefined";
        }
        System.out.print("Control/Mode Change");
        System.out.print('\t');
        System.out.print("Control Function: ");
        System.out.print(controlFunction);
        if (!"---".equals(thirdByteValue))
        {
            System.out.print('\t');
            System.out.print("Value: ");
            System.out.print(thirdByteValue);
            if (!"---".equals(usedAs))
            {
                System.out.print('\t');
                System.out.print("Used As: ");
                System.out.print(usedAs);
            }
        }
    }

    // http://www.recordingblogs.com/sa/tabid/88/Default.aspx?topic=MIDI+System+Exclusive+message
    private static void printSysexMessage(SysexMessage message)
    {
        System.out.print("Sysex message");
        System.out.print('\t');

        switch (message.getStatus())
        {
        case SysexMessage.SPECIAL_SYSTEM_EXCLUSIVE: // 0xF7, 247
            System.out.print("Special System Exclusive");
            break;
        case SysexMessage.SYSTEM_EXCLUSIVE: // 0xF0, 240
            System.out.print("System Exclusive");
            break;
        default:
            System.out.print("Unknown Sysex Message");
        }
    }

    private static void printMetaMessage(MetaMessage message)
    {
        System.out.print("Meta message");
        System.out.print('\t');

        byte[] data = message.getData();
        ByteBuffer buffer;

        // http://mido.readthedocs.org/en/latest/meta_message_types.html
        // http://www.recordingblogs.com/sa/tabid/88/Default.aspx?topic=MIDI+meta+messages
        switch(message.getType())
        {
        case 0: // 0x00
            // Sequence Number
            // http://www.recordingblogs.com/sa/tabid/88/Default.aspx?topic=MIDI+Sequence+Number+meta+message
            System.out.print("Sequence Number: ");
            buffer = ByteBuffer.wrap(data);
            short sequenceNumber = buffer.getShort();
            System.out.print(sequenceNumber);
            break;
        case 1: // 0x01
            // Text
            // http://www.recordingblogs.com/sa/tabid/88/Default.aspx?topic=MIDI+Text+meta+message
            System.out.print("Text:");
            System.out.print(new String(data));
            break;
        case 2: // 0x02,
            // Copyright
            // http://www.recordingblogs.com/sa/tabid/88/Default.aspx?topic=MIDI+Copyright+Notice+meta+message
            System.out.print("Copyright: ");
            System.out.print(new String(data));
            break;
        case 3: // 0x03
            // Track Name
            // http://www.recordingblogs.com/sa/tabid/88/Default.aspx?topic=MIDI+Track+Name+meta+message
            System.out.print("Track Name: ");
            System.out.print(new String(data));
            break;
        case 4: // 0x04
            // Instrument Name
            // http://www.recordingblogs.com/sa/tabid/88/Default.aspx?topic=MIDI+Instrument+Name+meta+message
            System.out.print("Instrument Name: ");
            System.out.print(new String(data));
            break;
        case 5: // 0x05
            // Lyrics
            // http://www.recordingblogs.com/sa/tabid/88/Default.aspx?topic=MIDI+Lyrics+meta+message
            System.out.print("Lyrics: ");
            System.out.print(new String(data));
            break;
        case 6: // 0x06
            // Marker
            // http://www.recordingblogs.com/sa/tabid/88/Default.aspx?topic=MIDI+Marker+meta+message
            System.out.print("Marker: ");
            System.out.print(new String(data));
            break;
        case 7: // 0x07
            // Cue Point
            // http://www.recordingblogs.com/sa/tabid/88/Default.aspx?topic=MIDI+Cue+Point+meta+message
            System.out.print("Cue Point: ");
            System.out.print(new String(data));
            break;
        case 9: // 0x09
            // Device Name
            System.out.print("Device Name: ");
            System.out.print(new String(data));
            break;
        case 32: // 0x20
            // Channel Prefix
            // http://www.recordingblogs.com/sa/tabid/88/Default.aspx?topic=MIDI+Channel+Prefix+meta+message
            System.out.print("Channel Prefix");
            System.out.print('\t');
            System.out.print("Channel: ");
            System.out.print(data[0]);
            break;
        case 33: // 0x21
            // Midi Port
            System.out.print("Midi Port");
            System.out.print('\t');
            System.out.print("Port: ");
            System.out.print(data[0]);
            break;
        case 47: // 0x2F
            // End of Track
            // http://www.recordingblogs.com/sa/tabid/88/Default.aspx?topic=MIDI+End+Of+Track+meta+message
            System.out.print("End of Track");
            break;
        case 81: // 0x51
            // Set Tempo
            // http://www.recordingblogs.com/sa/tabid/88/Default.aspx?topic=MIDI+Set+Tempo+meta+message
            System.out.print("Set Tempo");
            System.out.print('\t');

            // data[0], data[1], data[2] combine to make the tempo value
            // The tempo value is the number of microseconds per quarter note.
            byte[] tempoData = new byte[4];
            tempoData[0] = 0;
            tempoData[1] = data[0];
            tempoData[2] = data[1];
            tempoData[3] = data[2];
            buffer = ByteBuffer.wrap(tempoData);
            int tempo = buffer.getInt();

            // Since there are 60,000,000 microseconds per minute,
            // 60,000,000 / tempo = quarter notes per minute (beats per minute)
            int bpm = 60000000 / tempo;

            System.out.print("Tempo: ");
            System.out.print(tempo);
            System.out.print('\t');
            System.out.print("Beats Per Minute: ");
            System.out.print(bpm);
            break;
        case 84: // 0x54
            // SMPTE Offset
            // http://www.recordingblogs.com/sa/tabid/88/Default.aspx?topic=MIDI+SMPTE+Offset+meta+message
            System.out.print("SMPTE Offset");
            System.out.print('\t');

            byte hoursByte = data[0];
            byte minutes = data[1];
            byte seconds = data[2];
            byte frames = data[3];
            byte subframes = data[4];

            // bitmask to get first two bits of hoursByte
            int fpsValue = hoursByte & 0xC0; // 11000000

            // bitmask to get last six bits of hoursByte
            int hours = hoursByte & 0x3F; // 00111111

            double framesPerSecond = 0.0;
            if (fpsValue == 0xC0) // 11 000000
            {
                framesPerSecond =  30.0;
            }
            else if (fpsValue == 0x80) // 10 000000
            {
                framesPerSecond = 29.97;
            }
            else if (fpsValue == 0x40) // 01 000000
            {
                framesPerSecond = 25.0;
            }
            else if (fpsValue == 0x00) // 00 00000
            {
                framesPerSecond = 24.0;
            }
            else
            {
                System.out.print("Unknown SMPTE Offset");
                break;
            }

            System.out.print("Frames Per Second: ");
            System.out.print(framesPerSecond);
            System.out.print('\t');
            System.out.print("Hours: ");
            System.out.print(hours);
            System.out.print('\t');
            System.out.print("Minutes: ");
            System.out.print(minutes);
            System.out.print('\t');
            System.out.print("Seconds: ");
            System.out.print(seconds);
            System.out.print('\t');
            System.out.print("Frames: ");
            System.out.print(frames);
            System.out.print('\t');
            System.out.print("Sub-Frames: ");
            System.out.print(subframes);

            break;
        case 88: // 0x58
            // Time signature
            // http://www.recordingblogs.com/sa/tabid/88/Default.aspx?topic=MIDI+Time+Signature+meta+message
            System.out.print("Time Signature: ");

            // Time signature is data[0] / 2 ^ data[1]
            System.out.print(data[0]);
            System.out.print("/");
            System.out.print(Math.pow(2, data[1]));

            System.out.print('\t');
            System.out.print("Clocks per Click: ");
            System.out.print(data[2]);
            System.out.print('\t');
            System.out.print("Notated 32nd Notes Per Beat: ");
            System.out.print(data[3]);
            break;
        case 89: // 0x59
            // Key Signature
            // http://www.recordingblogs.com/sa/tabid/88/Default.aspx?topic=MIDI+Key+Signature+meta+message
            // If there is no key signature message, C major is assumed.

            System.out.print("Key Signature: ");

            // data[1]: Scale
            // Value of 0 or 1. 0 if scale is major, 1 if scale is minor.
            if (data[1] == 0)
            {
                // Major Scale

                // data[0]: Number of flats (if negative) or sharps (if positive)
                // Has values from -7 to 7
                switch (data[0])
                {
                // First note listed is the root note
                case -7:
                    // 7 flats: Cb, Db, Eb, Fb, Gb, Ab, Bb
                    System.out.print("Cb");
                    break;
                case -6:
                    // 6 flats: Gb, Ab, Bb, Cb, Db, Eb, F
                    System.out.print("Gb");
                    break;
                case -5:
                    // 5 flats: Db, Eb, F, Gb, Ab, Bb, C
                    System.out.print("Db");
                    break;
                case -4:
                    // 4 flats: Ab, Bb, C, Db, Eb, F, G
                    System.out.print("Ab");
                    break;
                case -3:
                    // 3 flats: Eb, F, G, Ab, Bb, C, D
                    System.out.print("Eb");
                    break;
                case -2:
                    // 2 flats: Bb, C, D, Eb, F, G, A
                    System.out.print("Bb");
                    break;
                case -1:
                    // 1 flat: F, G, A, Bb, C, D, E
                    System.out.print("F");
                    break;
                case 0:
                    // No flats or sharps: C, D, E, F, G, A, B
                    System.out.print("C");
                    break;
                case 1:
                    // 1 sharp: G, A, B, C, D, E, F#
                    System.out.print("G");
                    break;
                case 2:
                    // 2 sharps: D, E, F#, G, A, B, C#
                    System.out.print("D");
                    break;
                case 3:
                    // 3 sharps: A, B, C#, D, E, F#, G#
                    System.out.print("A");
                    break;
                case 4:
                    // 4 sharps: E, F#, G#, A, B, C, D#
                    System.out.print("E");
                    break;
                case 5:
                    // 5 sharps: B, C#, D#, E, F#, G#, A#
                    System.out.print("B");
                    break;
                case 6:
                    // 6 sharps: F#, G#, A#, B, C#, D#, E#
                    System.out.print("F#");
                    break;
                case 7:
                    // 7 sharps: C#, D#, E#, F#, G#, A#, B#
                    System.out.print("C#");
                    break;
                default:
                    System.out.print("Unknown key signature");
                }
            }
            else if (data[1] == 1)
            {
                // Minor Scale

                switch (data[0])
                {
                // First note listed is the root note
                case -7:
                    // 7 flats: Ab, Bb, Cb, Db, Eb, Fb, Gb
                    System.out.print("Abm");
                    break;
                case -6:
                    // 6 flats: Eb, F, Gb, Ab, Bb, Cb, Db
                    System.out.print("Ebm");
                    break;
                case -5:
                    // 5 flats: Bb, C, Db, Eb, F, Gb, Ab
                    System.out.print("Bbm");
                    break;
                case -4:
                    // 4 flats: F, G, Ab, Bb, C, Db, Eb
                    System.out.print("Fm");
                    break;
                case -3:
                    // 3 flats: C, D, Ev, F, G, Ab, Bb
                    System.out.print("Cm");
                    break;
                case -2:
                    // 2 flats: G, A, Bb, C, D, Eb, F
                    System.out.print("Gm");
                    break;
                case -1:
                    // 1 flat: D, E, F, G, A, Bb, C
                    System.out.print("Dm");
                    break;
                case 0:
                    // No flats or sharps: A, B, C, D, E, F, G
                    System.out.print("Am");
                    break;
                case 1:
                    // 1 sharp: E, F#, G, A, B, C, D
                    System.out.print("Em");
                    break;
                case 2:
                    // 2 sharps: B, C#, D, E, F#, G, A
                    System.out.print("Bm");
                    break;
                case 3:
                    // 3 sharps: F#, G#, A, B, C#, D, E
                    System.out.print("F#m");
                    break;
                case 4:
                    // 4 sharps: C#, D#, E, F#, G#, A, B
                    System.out.print("C#m");
                    break;
                case 5:
                    // 5 sharps: G#, A#, B, C#, D#, E, F#
                    System.out.print("G#m");
                    break;
                case 6:
                    // 6 sharps: D#, E#, F#, G#, A#, B, C#
                    System.out.print("D#m");
                    break;
                case 7:
                    // 7 sharps: A#, B#, C#, D#, E#, F#, G#
                    System.out.print("A#m");
                    break;
                default:
                    System.out.print("Unknown key signature");
                }
            }
            else
            {
                System.out.print("Unknown key signature");
            }
            break;
        case 127: // 0x7F
            // Sequencer Specific
            // http://www.recordingblogs.com/sa/tabid/88/Default.aspx?topic=MIDI+Sequencer+Specific+meta+message
            System.out.print("Sequencer Specific");
            break;
        default:
            System.out.print("Unknown meta message");
        }
    }

    public static class EventContainer
    {
        private Sequence sequence;
        private Track track;
        private int trackNumber;
        private int trackIndex;
        private MidiEvent event;
        private MidiMessage message;

        public EventContainer(
                Sequence sequence,
                Track track,
                int trackNumber,
                int trackIndex,
                MidiEvent event,
                MidiMessage message)
        {
            this.sequence = sequence;
            this.track = track;
            this.trackNumber = trackNumber;
            this.trackIndex = trackIndex;
            this.event = event;
            this.message = message;
        }

        public Sequence getSequence()
        {
            return sequence;
        }

        public Track getTrack()
        {
            return track;
        }

        public int getTrackNumber()
        {
            return trackNumber;
        }

        public int getTrackIndex()
        {
            return trackIndex;
        }

        public MidiEvent getEvent()
        {
            return event;
        }

        public MidiMessage getMessage()
        {
            return message;
        }
    }
}

