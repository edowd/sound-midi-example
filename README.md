
This program gives an example of how to read MIDI files in Java.

The program will read a MIDI file and print out the MIDI messages.

## Build

```bash
mvn clean package
```

## Run

### File

If the MIDI file is on the local machine:

```bash
java -jar target/sound-midi.jar <filename>
```

### URL

If the MIDI file is a URL, add the `--url` tag.

```bash
java -jar target/sound-midi.jar --url <url>
```

